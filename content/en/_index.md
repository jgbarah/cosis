---
title: "Cosis: Cositas ricas"

description: "Some stuff I find interesting"
cascade:
  featured_image: '/images/gohugo-default-sample-hero-image.jpg'
---

## BabiaXR

Visualizing data in XR:

* [Website](https://babiaxr.gitlab.io)

## VizNet stuff

Virtual reality network visualizer:

* [VizNet demos by Eva & Pedro](https://sarehp.github.io/vrnetvis/demos/)

## Software dependencies as cities

Finding ways of visualizing dependencies of software modules:

* [When clicking on a building, its neighborhood (dependencies) raises](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/)
* [When clicking on a building, its neighborhood (dependencies) raises, shades of green](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/index_gradient.html)
* [Tree city](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/index_tree.html)
* [Tree city with shades of green](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/index_tree_gradient.html)
* [Tree city, layers at same height from the floor](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/index_quarter_levels_03.html)
* [Tree city, layers at same height from the floor, parameter for more or less compataction and layer height](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/index_quarter_levels_querystring.html?buildingsep=20&quarterheight=0.1)
* [Tree city, layers at same height from the floor, parameter for more or less compataction and layer height, same packages as wireframe](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/index_tree_wireframe.html?buildingsep=1&extra=2)


## Virto VR Programming Language

Degree thesis by Julián Sánchez (presented in July 2022):

* [Website](https://jdjuli.github.io/virto/)
* [Demo](https://jdjuli.github.io/virto/scenes/demos/demo22/)

## Long COVID

Communciation related to long COVID

* [Demo](https://oluisjuan.github.io/LongCovid-Pilot/)

----
This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
and [Hugo](https://gohugo.io), and can be built in under 1 minute.
Literally. It uses the [`ananke` theme](https://github.com/theNewDynamic/gohugo-theme-ananke)
which supports content on your front page.
Edit `/content/en/_index.md` to change what appears here. Delete `/content/en/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
