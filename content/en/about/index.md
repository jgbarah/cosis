---
title: "About"
description: "Just some stuff I find interesting."
featured_image: '/images/Victor_Hugo-Hunchback.jpg'
menu:
  main:
    weight: 1
---
{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}}

This is just a website I use to showcase some stuff that I found interesting. Use at your own risk.
