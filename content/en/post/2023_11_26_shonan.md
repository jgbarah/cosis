---
date: 2022-11-26
title: "Demos for Shonan 192 meeting"
description: "Some demos of our VR visualizations, for the Shonan 192 meeting on Augmented Software Visualization"
featured_image: "/images/shonan.png"
tags: ["demo"]
---

Some demos, which we prepared for the [Shonan 192 Meeting on Augmented Software Visualization](https://shonan-meeting-192.github.io/). They can be seen in desktop or mobile browsers, but they are designed for browswers supporting WebXR in XR devices (such as Quest or Pico):

* [Basic Codecity for JetUML](https://thesis-dlumbrer.gitlab.io/ist21-reprpckg/first_experiment/scenes/screen/jetuml2021.html), [source code](https://gitlab.com/thesis-dlumbrer/ist21-reprpckg/-/blob/main/first_experiment/scenes/screen/jetuml2021.html?ref_type=heads), by David Moreno, from his [PhD disssertation](https://thesis-dlumbrer.gitlab.io/).

[![Example of BabiaXR implementation of Codecity](/cosis/images/babiaxr-codecity.png "Example of BabiaXR implementation of Codecity")](https://thesis-dlumbrer.gitlab.io/ist21-reprpckg/first_experiment/scenes/screen/jetuml2021.html)

* [Data in shelves for the CHAOSS project](https://jgbarah-forks.gitlab.io/emse22-scenes/dashboards/chaoss-dashboards-static.html), [source code](https://gitlab.com/jgbarah-forks/emse22-scenes/-/blob/main/scenes/dashboards/chaoss-dashboards-static.html), by David Moreno, from his [PhD disssertation](https://thesis-dlumbrer.gitlab.io/).

[![Example of data about a software project organized in shelves, using BabiaXR](/cosis/images/babiaxr-shelves.png "[Example of data about a software project organized in shelves, using BabiaXR")](https://jgbarah-forks.gitlab.io/emse22-scenes/dashboards/chaoss-dashboards-static.html)

* [Evolving Codecity](https://jgbarah-forks.gitlab.io/sattose2023/). [source code](https://gitlab.com/jgbarah-forks/sattose2023/-/blob/main/scenes/index.html), by David Moreno, from his  [PhD disssertation](https://thesis-dlumbrer.gitlab.io/).

[![Example of an evolving project with BabiaXR codecity implementation](/cosis/images/babiaxr-boats.gif "Example of an evolving project with BabiaXR codecity implementation")](https://jgbarah-forks.gitlab.io/sattose2023/)

* [Elevated city, showing npm dependencie of some software projects](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/experiment.html), [source code](https://gitlab.com/babiaxr/aframe-babia-components/-/blob/master/examples/boats/boats_dependency/experiment.html?ref_type=heads), by David Moreno, from his  [PhD disssertation](https://thesis-dlumbrer.gitlab.io/).

[![Example of an "elevated city", showing dependencies for an npm package, implemented with BabiaXR](/cosis/images/babiaxr-elevated-city.gif "Example of an elevated city, showing dependencies for an npm package, implemented with BabiaXR")](https://babiaxr.gitlab.io/aframe-babia-components/examples/boats/boats_dependency/experiment.html)

<!--
* [The Virto Programming Language and Environment (PoC)](https://jdjuli.github.io/virto/scenes/demos/demo22/), by Julian Sanchez-Fernandez, from his degree thesis.

[![A program in the Virto Programming Language (proof of concept)](/cosis/images/virto.png "A program in the Virto Programming Language (proof of concept")](https://jdjuli.github.io/virto/scenes/demos/demo22/)
-->

* Virtual reality network visualizer (VRNetVis): [traceroute trace](https://mobiquo.gsyc.urjc.es/traceroute/), [TCP trace](https://mobiquo.gsyc.urjc.es/tcp/), by Eva Castro and Pedro de las Heras, based on a degree thesis by Alejandro Esteban-Lopez.

[![Example of a network trace animated in VRNetVis](/cosis/images/vrnetvis.gif "Example of a network trace animated in VRNetVis")](https://mobiquo.gsyc.urjc.es/traceroute/)
